import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;

import org.bouncycastle.jce.provider.BouncyCastleProvider;



public class BouncyCrypto {
	static Cipher cipher;
	public static void main(String[] args) throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter text to be encrypted");
		String plainText = sc.next();
		sc.close();
		
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		SecretKey secretKey = keyGenerator.generateKey();
		cipher = Cipher.getInstance("AES");
		String encryptedText = encrypt(plainText, secretKey);
		String decryptedText = decrypt(encryptedText, secretKey);
		System.out.println("Decrypted Text After Decryption AES: " + decryptedText);
		
		keyGenerator = KeyGenerator.getInstance("Blowfish");
		keyGenerator.init(128);
		secretKey = keyGenerator.generateKey();
		cipher = Cipher.getInstance("Blowfish");
		encryptedText = encrypt(plainText, secretKey);
		decryptedText = decrypt(encryptedText, secretKey);
		System.out.println("Decrypted Text After Decryption Blowfish: " + decryptedText);
		
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		KeyPair keyPair = kpg.generateKeyPair();
		cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
		SealedObject encyptedMessage = new SealedObject( plainText, cipher);
		cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
		decryptedText = (String) encyptedMessage.getObject(cipher);
		System.out.println("Decrypted Text After Decryption RSA: " + decryptedText);
	
	    Signature signature = Signature.getInstance("SHA1withRSA", "BC");
	    signature.initSign(keyPair.getPrivate(), new SecureRandom());
	    byte[] message = plainText.getBytes();
	    signature.update(message);
	    byte[] sigBytes = signature.sign();
	    signature.initVerify(keyPair.getPublic());
	    signature.update(message);
	    System.out.println("RSA Sign Verifaction: " + signature.verify(sigBytes));
	    
	    //extra credit
	    // generate array of 100 random Strings (uses alpha-numeric characters)
	    String alphabetString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	    String[] randomStrings = new String[100];
	    Arrays.fill(randomStrings, "");
	    int strLength = 10;
	    Random random = new Random();
	    for(int i = 0; i < randomStrings.length; i++) {
	    	for(int j = 0; j < strLength; j++) {
	    		randomStrings[i] += alphabetString.charAt(random.nextInt(alphabetString.length()));
	    	}
	    }
	    
	    // encrypt 100 strings with AES
	    keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		secretKey = keyGenerator.generateKey();
		cipher = Cipher.getInstance("AES");
	    long startTimeAES = System.nanoTime();
	    for(String string : randomStrings) {
	    	encrypt(string, secretKey);
	    }
	    long endTimeAES = System.nanoTime();
	    long AEStime =  endTimeAES - startTimeAES;
	    
	    // encrypt 100 strings with Blowfish
	    keyGenerator = KeyGenerator.getInstance("Blowfish");
		keyGenerator.init(128);
		secretKey = keyGenerator.generateKey();
		cipher = Cipher.getInstance("Blowfish");
	    long startTimeBlowfish = System.nanoTime();
	    for(String string : randomStrings) {
	    	encrypt(string, secretKey);
	    }
	    long endTimeBlowfish = System.nanoTime();
	    long BlowfishTime = endTimeBlowfish - startTimeBlowfish;
	    
	    // encrypt 100 strings with RSA
	    kpg = KeyPairGenerator.getInstance("RSA");
		keyPair = kpg.generateKeyPair();
		cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
	    long startTimeRSA = System.nanoTime();
	    for(String string : randomStrings) {
			new SealedObject( string, cipher);
	    }
	    long endTimeRSA = System.nanoTime();
	    long RSAtime =  endTimeRSA - startTimeRSA; 

	    System.out.println("AES is " + (double)RSAtime/(double)AEStime  + " time faster than RSA");
	    System.out.println("Blowfish is " + (double)RSAtime/(double)BlowfishTime  + " time faster than RSA");
	    System.out.println("Blowfish is " + (double)AEStime/(double)BlowfishTime  + " time faster than AES");
	}
	
	public static String encrypt(String plainText, SecretKey secretKey)
			throws Exception {
		byte[] plainTextByte = plainText.getBytes();
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] encryptedByte = cipher.doFinal(plainTextByte);
		String encryptedText = DatatypeConverter.printBase64Binary(encryptedByte);
		return encryptedText;
	}

	public static String decrypt(String encryptedText, SecretKey secretKey)
			throws Exception {
		byte[] encryptedTextByte = DatatypeConverter.parseBase64Binary(encryptedText);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		return decryptedText;
	}
	
}
